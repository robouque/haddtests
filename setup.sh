export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS

lsetup cmake

ROOT_CVMFS="/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.22.08"
#ROOT_CVMFS="/cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.22.06"
# look at ${ROOT_CVMFS}/src/io/io/src/TFileMerger.cxx

source ${ROOT_CVMFS}/x86_64-centos7-gcc48-opt/bin/thisroot.sh

# create build directory if does not exists aready
mkdir -p build 

export PATH="$PWD/build/:$PATH"
export LD_LIBRARY_PATH="$PWD/build/:$LD_LIBRARY_PATH"